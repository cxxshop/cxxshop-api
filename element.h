#ifndef ELEMENT_H
#define ELEMENT_H

#include "api/define.h"

#include <string>
#include <atomic>

namespace nstd::impl
{
class TimeImpl;
}

namespace nstd::element
{

/* Time */

class DLLEXPORT Time
{
  public:
    explicit Time();
    ~Time();

    /* interface */
    std::string timestamp();

  private:
    nstd::impl::TimeImpl *m_impl;
};

/* ReferenceCounter */

//class DLLEXPORT ReferenceCounter
//{
//  public:
//    ReferenceCounter();
//
//    ReferenceCounter &increase();
//    ReferenceCounter &decrease();
//
//    nstd::type::ull get_number() const;
//
//  private:
//    std::atomic<nstd::type::ull> m_counter;
//};
} // namespace nstd::element

#endif // ELEMENT_H