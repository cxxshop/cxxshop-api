#ifndef DEFINE_H
#define DEFINE_H

#ifdef _WIN32
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT __attribute__((visibility("default")))
#endif

namespace nstd::type
{
using ull = unsigned long long;
}

namespace nstd::element
{

} // namespace nstd::element

namespace nstd::kit
{
enum class LogLevel
{
    i, // information
    e, // error
    w, // warning
    d, // debug
    f  // fatal
};

} // namespace nstd::kit

#endif // DEFINE_H