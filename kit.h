#ifndef KIT_H
#define KIT_H

#include "api/define.h"

#include <functional>
#include <future>

namespace nstd::impl
{
class ThreadPoolImpl;
class PointerImpl;
} // namespace nstd::impl

namespace nstd::kit
{

/* Log */

class DLLEXPORT Log
{
  public:
    static Log *instance();

    Log(const Log &) = delete;
    Log &operator=(const Log &) = delete;

    virtual ~Log() = default;

    template <class... Args> virtual void i(Args... &&) = 0;
    template <class... Args> virtual void e(Args... &&) = 0;
    template <class... Args> virtual void w(Args... &&) = 0;
    template <class... Args> virtual void d(Args... &&) = 0;
    template <class... Args> virtual void f(Args... &&) = 0;

    virtual int start(const char *log_path, int file_limit, int text_limit) = 0;
    virtual int start(std::function<void(LogLevel, const char *)>) = 0;
    virtual void stop() = 0;

  private:
    Log() = default;
};

/* ThreadPool */

class DLLEXPORT ThreadPool
{
  public:
    explicit ThreadPool(nstd::ull capacity);
    ~ThreadPool();

    template <class F, class... Args>
    auto add_task(F &&, Args &&...) -> std::future<typename std::result_of<F(Args...)>::type>;
    nstd::ull active_task_number() const;
    int add_capacity(nstd::ull more_capacity);

  private:
    ThreadPoolImpl *m_impl;
};

/* Pointer */

template <class T> class DLLEXPORT Pointer
{
  public:
    Pointer(T *pointer, std::function<void(T *)> deleter = nullptr);
    Pointer(const Pointer &);
    ~Pointer();

    T &operator*();
    T *operator->();
    Pointer &operator=(const Pointer &);
    Pointer &operator=(Pointer &&);
    Pointer &operator=(T *pointer);

    template <class U> Pointer &from_derived(U *pointer);
    template <class U> Pointer &from_base(U *pointer);
    Pointer shadow();
    bool occupied() const;

  private:
    PointerImpl<T> *m_impl;
};

/*  */

/*  */

/*  */

/*  */

/*  */

/*  */

/*  */

/*  */
} // namespace nstd::kit

#endif // KIT_H