#ifndef DATA_H
#define DATA_H

#include "api/define.h"

#include <initializer_list>

namespace nstd::impl
{
class QueueImpl;
class StackImpl;
class ArrayImpl;
class TensorImpl;
} // namespace nstd::impl

namespace nstd::data
{

/* [utility] print */

template <class T> int print(const Queue<T> &);
template <class T> int print(const Stack<T> &);
template <class T> int print(const Array<T> &);
template <class T> int print(const Tensor<T> &);

/* Queue */

template <class T> class Queue
{
  public:
    Queue();
    Queue(const Queue &);
    Queue(Queue &&);
    Queue(std::initializer_list<T>);

    Queue &operator=(const Queue &);
    Queue &operator=(Queue &&);
    bool operator==(const Queue &);
    bool operator!=(const Queue &);

    T pop();
    Queue &push(T &&);
    template <class... Args> Queue &emplace(Args... &&);
    Queue &for_each(std::function<void(const T &)>);

    T front() const;
    T back() const;
    ull size() const;

  private:
    QueueImpl<T> *m_impl;
};

/* Stack */

template <class T> class Stack
{
  public:
    Stack();
    Stack(const Stack &);
    Stack(Stack &&);
    Stack(std::initializer_list<T>);

    Stack &operator=(const Stack &);
    Stack &operator=(Stack &&);
    bool operator==(const Stack &);
    bool operator!=(const Stack &);

    T pop();
    Stack &push(T &&);
    template <class... Args> Stack &emplace(Args... &&);
    Stack &for_each(std::function<void(const T &)>);

    T top() const;
    ull size() const;

  private:
    StackImpl *m_impl;
};

/* Array */

template <class T> class Array
{
  public:
    Array();
    Array(ull size);
    Array(ull size, T &&);
    Array(const Array &);
    Array(Array &&);
    Array(std::initializer_list<T>);

    Array &operator=(const Array &);
    Array &operator=(Array &&);
    bool operator==(const Array &);
    bool operator!=(const Array &);
    T &operator[](ull index);

    Array &insert(ull index, T &&);
    Array &erase(ull index);
    Array &remove(const T &);

    Array &push(T &&);
    template <class... Args> Array &emplace(Args... &&);
    Array &for_each(std::function<void(const T &)>);

    ull find_first(const T &) const;
    ull find_last(const T &) const;
    ull find_first_from(ull start, const T &) const;
    ull find_last_util(ull finish, const T &) const;
    bool contain(const T &) const;

    Array &reverse();

  private:
    ArrayImpl *m_impl;
};

/* Tensor */

template <class T> class Tensor
{
  public:
    Tensor();
    template <class... Dimension> Tensor(Dimension... &&);
    Tensor(ull size, T &&);
    Tensor(const Array &);
    Tensor(Array &&);
    Tensor(std::initializer_list<T>);

  private:
    TensorImpl *m_impl;
};

} // namespace nstd::data

#endif // DATA_H